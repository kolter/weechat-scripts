#!/bin/sh

TAR=tar
DIR=$(basename $(pwd))
SNAPSHOT="${1}"

rm -f "../${SNAPSHOT}"

${TAR} \
    --exclude ".git" \
    --exclude "debian" \
    --transform 's,^\.\./,,' \
    -czvf ../${SNAPSHOT} ../${DIR}
